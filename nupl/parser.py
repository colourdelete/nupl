class parser():
  def __init__(self, raw):
    self.raw = raw
    self.pointer = [0, ]
  def _open_group(self):
    self.pointer.append(0)
  def _close_group(self):
    self.pointer.append(0)
  def _move_group(self, offset=1):
    self.pointer[-1] = offset
  def parse(self):
    ignore  = []
    ignore_ = False
    for i in range(len(raw)):
      char = raw[i]
      if char == '\'' and raw[i-1] != '\\':
        ignore_ = not ignore_
      ignore.append(ignore_)
    del ignore_
    data = []
    for i in range(len(raw)):
      char = raw[i]
      if ignore[i]:
        continue
      elif char == '(':
        self._open_group()
      elif char == ')':
        self._close_group()
    return ignore, data

# import re
# class parser():
#   def __init__(self, chars=''):
#     self.chars = chars
#     self.ignore_map = []
#
#   def _split_semicolon(self):
#     pattern = re.compile(r'''((?:[^;\n"']|"[^"]*"|'[^']*')+)''')
#     return pattern.split(self.chars)[1:]
#
#   def _get_ignore_map(self):
#     self.ignore_map = []
#     for i in range(len(self.chars)):
#       ignore = False
#       char = self.chars[i]
#       if char == '\'' and self.chars[i-1] != '\\':
#         ignore = not ignore
#       elif char == '"' and self.chars[i-1] != '\\':
#         ignore = not ignore
#       self.ignore_map.append(ignore)


if __name__ == '__main__':
  print('parser')
  print('input to "EOF"')
  text = ''
  i = 1
  inp = input('{} '.format(i))
  if inp != 'EOF':
    text += inp.replace('\EOF', 'EOF')
    text += '\n'
  while inp != 'EOF':
    i += 1
    inp = input('{} '.format(i))
    if inp != 'EOF':
      text += inp.replace('\EOF', 'EOF')
      text += '\n'
  inst = parser(text)
  result = inst.parse()
  print(result)
  # print(inst._split_semicolon())